# About
In this project registered user can:
* See a bunch of markers which represent cars in the map
* Filter those cars by few properties
* Filter cars by distance from an address selected with Google Places Autocomplete input.
* See new cars appear in real time. 
* Create a new car. 
* Look at list of cars. Each car has its bookings listed too.
* Book a car. 
* End car booking.

There might be some errors in the console.
# Setup
To set up this project run `npm install` in the root directory of the project
# Run locally
To run this project locally run `meteor` in the root directory of the project