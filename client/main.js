import '../imports/client/routes';
import '../imports/client/templates/index';

Template.layout.onCreated(function helloOnCreated() {

  Meteor.subscribe('myCars');
  Meteor.subscribe('myBookings');
  Meteor.subscribe('myClientEmails');

  Tracker.autorun(()=>{
    if ( !Meteor.userId() ){
      FlowRouter.go('/');
    }
  })

});

Template.layout.onRendered(function() {
})

Template.layout.helpers({
  logged_in:function(){
    return !!Meteor.userId();
  }
});

Template.layout.events({
  'click button.home':()=>{
    FlowRouter.go('/');
  },
  'click button.mycars':()=>{
    FlowRouter.go('/mycars');
  },

});
