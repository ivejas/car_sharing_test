import {Cars} from '../both/collections/cars.collection';
import {Bookings} from '../both/collections/bookings.collection';

Meteor.publish('availableCars', function(params){

  this.autorun(()=>{
    
    let booking_ids = Bookings.find({cancelled_at: {$exists:false}, expires_at: {$lt:+ new Date}}).fetch().map( booking => { return booking._id } );
    let query =Object.assign({_id:{$nin:booking_ids}}, params);

    let cursor = Cars.find(query, { noReady: true });

    return cursor;

  });
})

Meteor.publish('currentCar', function(){
  if(this.userId){
    this.autorun(()=>{
      let car_id = Bookings.findOne({cancelled_at: {$exists:false}, user_id:this.userId, expires_at: {$lt:+ new Date}}).car_id;
      let cursor = Cars.find({_id:car_id});
      return cursor;
    })
  }
})


Meteor.publish('bookings', function(){
  return Bookings.find({cancelled_at: {$exists:false}, expires_at: {$lt:+ new Date}}, {fields:{
    car_id:1, cancelled_at: 1, expires_at: 1
  }});
})


Meteor.publish('currentBooking', function(){
  if(this.userId){
      return Bookings.find({cancelled_at: {$exists:false}, user_id:this.userId, expires_at: {$lt:+ new Date}});
  }
})


Meteor.publish('myCars', function(){
  if(this.userId){
    return Cars.find({user_id:this.userId});
  }
})


Meteor.publish('myBookings', function(){
  if(this.userId){
    return Bookings.find({user_id:this.userId});
  }
})

Meteor.publish('myCarBookings', function(){
  if(this.userId){
    this.autorun(()=>{
      let car_ids = Cars.find({user_id:this.userId}).fetch().map( booking => { return booking._id } );
      return Bookings.find({car_id:{$in:car_ids}});
    });
  }
})

Meteor.publish('myClientEmails', function(){
  if(!this.userId){return false;}
    this.autorun(()=>{
      let car_ids = Cars.find({user_id:this.userId}).fetch().map( car => {return car._id});

      let user_ids = Bookings.find({car_id:{$in:car_ids}}).fetch().map( user => { return user.user_id } );

      return Meteor.users.find({_id:{$in:user_ids}}, {fields: {'emails.address': 1}});
    });
})



