import {Bookings} from '../both/collections/bookings.collection';
import {Cars, CarSchema} from '../both/collections/cars.collection';
import {getUserActiveBooking, getCarActiveBooking} from './helpers';

Meteor.methods({
  bookCar(car_id, expires_at){

    let car = Cars.findOne({_id:car_id});

    if(!this.userId){throw new Meteor.Error('user-not-logged-in', "User is not logged in");} 
    if(getUserActiveBooking(this.userId)){throw new Meteor.Error('already-has-booked-car', "User can only book a single car at a time")};
    if(getCarActiveBooking(car_id)){throw new Meteor.Error('car-already-booked', "This car is already booked")};
    if(this.userId == car.user_id){throw new Meteor.Error('cant-book-owned-car', "User can not book his own car")};

    Bookings.insert({
      user_id: this.userId,
      car_id,
      expires_at,
      created_at: + new Date
    });
  },

  insertCar(data){
    if(!this.userId){
      throw new Meteor.Error('user-not-logged-in', "User is not logged in");
    }
    data.user_id = this.userId;
    data.location = { type: "Point", coordinates: data.location }
    CarSchema.validate(data);
    return Cars.insert(data);
  },

  endBooking(location){
    if(!this.userId){
      throw new Meteor.Error('user-not-logged-in', "User is not logged in");
    }

    //TODO: Validate booking owner

    let booking = getUserActiveBooking();
    booking.cancelled_at = + new Date();

    
    // let lat = Math.floor(Math.random() * 181) - 90;
    // let lng = Math.floor(Math.random() * 361) - 180;

    Cars.update({_id:booking.car_id}, {$set:{location: { type: "Point", coordinates: location }}});
    return Bookings.update({_id:booking._id}, {$set:booking});

  }
});
