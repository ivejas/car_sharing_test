import '../both/collections/bookings.collection';
import '../both/collections/cars.collection';
import './publications';
import './indexes';
import './methods';