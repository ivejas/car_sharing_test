import {Bookings} from '../both/collections/bookings.collection';
import { Cars } from '../both/collections/cars.collection';

export function getUserActiveBooking(user_id){
  let bookings = Bookings.find({cancelled_at:{$exists: false}, user_id, expires_at:{$gt:+ new Date()}}).fetch();
  if(bookings.length){
    return bookings[0];
  }
  
  return false
}

export function getUserActiveCar(user_id){
  let booking = getUserActiveBooking(user_id);
  if( booking ){
    return Cars.find({_id: booking.car_id});
  }
  return false;
}


export function getCarActiveBooking(car_id){
  let bookings = Bookings.find({cancelled_at:{$exists: false}, car_id, expires_at:{$gt:+ new Date()}}).fetch();
  if(bookings.length){
    return bookings[0];
  }
  return false;
}