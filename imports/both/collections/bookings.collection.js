import SimpleSchema from 'simpl-schema';

Bookings = new Meteor.Collection( 'booking' );

let BookingSchema = new SimpleSchema({
  user_id: {
    type: String,
    label: "User ID",
  },
  car_id:{
    type: String,
    label: "Car ID",
  },
  created_at:{
    type: Number,
    label: "Timestamp of creation date"
  },
  expires_at:{
    type: Number,
    label: "Timestamp of expiration date"
  },
  cancelled_at:{
    type: Number,
    label: "Timestamp of cancellation date",
    optional: true
  }
});

Bookings.attachSchema( BookingSchema ); 

Bookings.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export  {Bookings, BookingSchema};