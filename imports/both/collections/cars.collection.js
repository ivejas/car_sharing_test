import SimpleSchema from 'simpl-schema';

Cars = new Meteor.Collection( 'car' );

let CarSchema = new SimpleSchema({
  user_id:{
    type: String,
    label: "Owner ID"
  },
  name: {
    type: String,
    label: "Name",
  },
  fuel:{
    type: Number,
    label: "Fuel percentage",
    min: 0,
    max: 100
  },
  price:{
    type: Number,
    label: "Price"
  },
  type:{
    type: String,
    label: "Type"
  },
  location:Object,
  'location.type':String,
  'location.coordinates':Array,
  'location.coordinates.$':Number
  }
);

Cars.attachSchema( CarSchema ); 
Cars.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
export {Cars, CarSchema};