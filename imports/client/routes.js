import {Bookings} from '../both/collections/bookings.collection';
FlowRouter.route( '/', {
  action: function() {
    BlazeLayout.render( 'layout', {content:'home'}); 
  }
});

FlowRouter.route( '/create', {
  action: function() {
    if(Meteor.userId()){
      BlazeLayout.render( 'layout', {content:'carCreate'}); 
    } else {
      BlazeLayout.render( 'layout', {content:'map'}); 
    }
  }
});

FlowRouter.route( '/mycars', {
  action: function() {
    if(Meteor.userId()){
      BlazeLayout.render( 'layout', {content:'carList'}); 
    } else {
      BlazeLayout.render( 'layout', {content:'map'}); 
    }
  }
});


FlowRouter.route( '/book/:id', {
  action: function(params) {
    if(Meteor.userId()){
      BlazeLayout.render( 'layout', {content:'bookCar',car:params.id}); 
    } else {
      BlazeLayout.render( 'layout', {content:'map'}); 
    }
  }
});