import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Cars } from '../../../both/collections/cars.collection';
import * as _ from 'lodash';


import './currentBooking.html';

Template.currentBooking.onCreated(function helloOnCreated() {
  this.data.location = []


});

Template.currentBooking.onRendered(function() {
  
  let input = new google.maps.places.Autocomplete(
    (document.getElementById('autocomplete')), {types:['geocode']}
  )
  google.maps.event.addListener(input, 'place_changed', ()=>{
    
    let place = input.getPlace();

    if (!place.geometry) {
      return;

    }

    this.data.location = [place.geometry.location.lat(), place.geometry.location.lng()];

  });
})

Template.currentBooking.helpers({
  car: ()=>{
    let booking = Bookings.findOne({user_id:Meteor.userId(), cancelled_at:{$exists:false}, expires_at:{$gt: + new Date()}});
    return Cars.findOne({_id:booking.car_id});
  },

  booking: ()=>{
    return booking = Bookings.findOne({user_id:Meteor.userId(), cancelled_at:{$exists:false}, expires_at:{$gt: + new Date()}});
  }, 

  date: (timestamp)=>{
    return moment(timestamp).format('YYYY-MM-DD hh:mm');
  }
});

Template.currentBooking.events({
  'click .stop': (event, instance)=>{
    $('button#stop').prop('disabled', true);
    if(instance.data.location.length > 1){
      Meteor.call('endBooking', instance.data.location, (error, response)=>{
        if(!error){
          alert('Booking ended!');
        } else {
          alert(error.reason);
        }
      })
    } else {
      alert('Please enter new car location');
    }
  }
});
