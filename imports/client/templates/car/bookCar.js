import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {Session} from 'meteor/session';
import { Cars } from '../../../both/collections/cars.collection';
import * as _ from 'lodash';


import './bookCar.html';

Template.bookCar.onCreated(function carListOnCreated() {

  Meteor.subscribe('availableCars');
  Session.set('id', this.data.id);
});

Template.bookCar.onRendered(function() {
  let datepicker = this.$('.datetimepicker').datetimepicker({
    minDate: moment(new Date()).add(1, 'day')
  });
})

Template.bookCar.helpers({
  car: ()=>{
    let car = Cars.findOne({_id: FlowRouter.getParam('id')});    
    return car;
  },
});

Template.bookCar.events({
  'click .book':(event, instance)=>{

    
    let expires = + new Date($('.set-due-date').val()) ;

    if(!expires || expires < + new Date()) { alert("Please set a valid expiration date") ; return;}


    $('.book').prop('disabled', true);

    Meteor.call('bookCar', FlowRouter.getParam('id'), expires, (error, response)=>{
      $('.book').prop('disabled', false);

      if(!error){
        FlowRouter.go('/');
      } else {
        alert(error.reason);
      }
    })
  }
});
