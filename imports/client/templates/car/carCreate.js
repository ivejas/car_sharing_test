import { Template } from 'meteor/templating';
import { Cars } from '../../../both/collections/cars.collection';
import * as _ from 'lodash';


import './carCreate.html';

Template.carCreate.onCreated(function helloCarCreated() {

});


Template.carCreate.onRendered(function() {
  GoogleMaps.load({ key: 'AIzaSyD69Awm_2y8rqV6bQvteSSghW9FmZAOKaM', libraries: 'places' })

  let interval = setInterval(()=>{
    if(google){
      let input = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete')), {types:['geocode']}
      )
      google.maps.event.addListener(input, 'place_changed', ()=>{
        
        let place = input.getPlace();
    
        if (!place.geometry) {
          return;
        }
    
        $('input#lat').val(place.geometry.location.lat());
        $('input#lng').val(place.geometry.location.lng());
        
    
      });
      clearInterval(interval);
    }
  }, 20);
})

Template.carCreate.helpers({});

Template.carCreate.events({
  'submit #insert': (event, instance)=>{
    // increment the counter when button is clicked
    event.preventDefault();
    if(!formHasErrors()){
      $('button#submit').prop('disabled', true);
      Meteor.call('insertCar', getFormValues(), (error, response)=>{
        $('button#submit').prop('disabled', false);
        if(!error){
          alert("Success!")
          FlowRouter.go('/mycars');
        } else {
          alert(error.reason);
        }
      })
    }
  },

  'keyup/change input': (event, instance)=>{
    
    $(event.target).removeClass('is-invalid');

  }
});

function getFormValues(instance){

  let form_values = {};

  $('form input').each((index, value)=>{
    value = $(value)[0];
    if(value.type == "number"){
      form_values[value.id] = Number(value.value);
    } else {
      form_values[value.id] = value.value;
    }
  });

  form_values.location = [Number(form_values.lat), Number(form_values.lng),];

  delete(form_values.lat);
  delete(form_values.lng);
  delete(form_values.autocomplete);
  
  return form_values;
}

function formHasErrors(){
  // let form_values =  $('form input').map((index, value)=>{
  //   return {[$(value).id]: $(value).val()}
  // })

  let error = null

  if($('form input#fuel').val() > 100 || $('form input#fuel').val() < 0){
    error = "Fuel must be between 0 and 100";
    $('form input#price').addClass('is-invalid');
  }

  if( $('form input#price').val() < 0 ){
    error = "Price must be bigger than 0";
    $('form input#price').addClass('is-invalid');
  }

  if( +$('form input#lat').val() < -90 || +$('form input#lat').val() > 90 ){
    error = "Latitude must be between -90 and 90";
    $('form input#lat').addClass('is-invalid');
  }

  if( +$('form input#lng').val() < -180 || +$('form input#lat').val() > 180 ){
    error = "Longitude must be between -180 and 180";
    $('form input#lng').addClass('is-invalid');
  }


  $('form input').each((index, input)=>{
    if(!$(input).val()){
      $(input).addClass('is-invalid');
      error = 'Please fill all fields';
    }
    // if($(input).hasClass('is-invalid')){
    //   $(input).parent().addClass('was-validated');
    // }
  })

  if(error){
    alert(error)
    return true;
  } 

  return false;

}
