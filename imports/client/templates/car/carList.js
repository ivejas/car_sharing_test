import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Cars } from '../../../both/collections/cars.collection';
import * as _ from 'lodash';


import './carList.html';

Template.carList.onCreated(function carListOnCreated() {

  Meteor.subscribe('myCars');
  Meteor.subscribe('myCarBookings');

});

Template.carList.onRendered(function() {

})

Template.carList.helpers({
  cars: ()=>{
    let cars = Cars.find({user_id:Meteor.userId()}).fetch();    
    return cars;
  },

  bookings:(car_id)=>{
    return Bookings.find({car_id}).fetch();
  },

  user:(_id)=>{
    return Meteor.users.findOne({_id});
  },

  date:(timestamp)=>{
    return moment(timestamp).format('YYYY-MM-DD hh:mm');
  }
});

Template.carList.events({
  
  'click button.create':()=>{
    FlowRouter.go('/create');
  }
});
