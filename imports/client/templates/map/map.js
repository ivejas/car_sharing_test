import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Cars } from '../../../both/collections/cars.collection';
import escape from 'escape-string-regexp';
import * as _ from 'lodash';


import './map.html';

Template.map.onCreated(function helloOnCreated() {

  this.data.markersArray = [];
  this.data.search = {}
  this.data.infoWindows = [];

  this.data.sub = Meteor.subscribe('availableCars', {});
  this.data.sub2 = Meteor.subscribe('bookings');


  Tracker.autorun(()=>{
    drawMarkers(this);
    
  })

  GoogleMaps.ready('exampleMap', (map)=>{
    // Add a marker to the map once it's ready
    let cars = Cars.find(buildQuery(this.data.search)).fetch();
    
    if(!cars) cars = [];

    if(!this.data.markersArray) this.data.markersArray = [];
    if(!this.data.infoWindows) this.data.infoWindows = [];

    while(this.data.markersArray.length) { this.data.markersArray.pop().setMap(null); }

    for(let i = 0; i < cars.length; i++){
      let marker = new google.maps.Marker({
        position: {lat:cars[i].location.coordinates[0], lng: cars[i].location.coordinates[1]},
        map: map.instance
      });

      let infowindow = makeInfoWindow(map, cars[i]);
  
      this.data.markersArray.push(marker);
      this.data.infoWindows.push(infowindow);

      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
    }

    

    // init places input 
    this.data.placesInput = new google.maps.places.Autocomplete(
      (document.getElementById('autocomplete')), {types:['geocode']}
    )

    google.maps.event.addListener(this.data.placesInput, 'place_changed', ()=>{
      let place = this.data.placesInput.getPlace();

      if (!place.geometry) {
        return;
      }

      this.data.search = Object.assign(this.data.search, {lat:place.geometry.location.lat(), lng:place.geometry.location.lng()});

      clearMapResubscribe(this);

    });
  });

});

Template.map.onRendered(function() {
  GoogleMaps.load({ key: 'AIzaSyD69Awm_2y8rqV6bQvteSSghW9FmZAOKaM', libraries: 'places' })
})

Template.map.helpers({
  mapOptions: function() {
    // Make sure the maps API has loaded
    if (GoogleMaps.loaded()) {
      // Map initialization options
      return {
        center: new google.maps.LatLng(-37.8136, 144.9631),
        zoom: 1
      };
    }
  }
});

Template.map.events({
  'keyup/change .form-control': _.debounce((event, instance)=>{
    // increment the counter when button is clicked
    if(event.target.value){
      instance.data.search = Object.assign(instance.data.search, {[event.target.id]: event.target.value});
    } else {
      delete(instance.data.search[event.target.id]);
    }


    if(event.target.id == 'autocomplete' ){
      if(event.target.value == ""){
        $("#" + event.target.name).val('');
        instance.data.search.lat = instance.data.search.lng = null;
        $('#distance').val(10).prop('disabled', true);
      } else {
        $('#distance').prop('disabled', false).trigger('change');
      }
    }

    clearMapResubscribe(instance);
    drawMarkers(instance);

  }, 300),
  'click .book-a-car': (event, instance)=>{
    FlowRouter.go('/book/'+event.target.id);
  }
});

function clearMapResubscribe(instance){
  while(instance.data.markersArray.length) { instance.data.markersArray.pop().setMap(null); }
  instance.data.sub.stop();
  instance.data.sub = Meteor.subscribe('availableCars', buildQuery(instance.data.search));
}


function makeInfoWindow(map, car, hasBookedCars = null){
  
  let content = ""+
  "<ul class='list-group'>"+
  "<li class='list-group-item'> Name: "+ car.name +"</li>"+
  "<li class='list-group-item'> Price: "+ car.price +"</li>"+
  "<li class='list-group-item'> Fuel: "+ car.fuel +" % </li>"+
  "<li class='list-group-item'> Type: "+ car.type +"</li>"+
  // "<li> Name: "+ cars[i].name +"</li>"+
  "</ul>";
  if(Meteor.userId()){
    content = content + "<button class='btn btn-success book-a-car' id='"+car._id+"'>Book</button>"
  }


  return infowindow = new google.maps.InfoWindow({content});
}


function buildQuery(params){
  let booked_ids = Bookings.find({cancelled_at: {$exists:false}, expires_at: {$lt:+ new Date}}).fetch().map( booking => {return booking.car_id})
  let query = {_id:{$nin: booked_ids}, user_id:{$ne:Meteor.userId()}};
  if(params.name){
    query.name = {$regex: escape(params.name), $options: 'i'};
  }
  if(params.price){
    query.price = {$lt: Number(params.price)};
  }
  if(params.fuel){
    query.fuel = {$gt: Number(params.fuel)}
  }
  if(params.type){
    query.type = {$regex: escape(params.type), $options: 'i'};
  }
  if(params.lat && params.lng && params.distance){
    query.location =  {$near:{
      $geometry: {
        type: 'Point',
        coordinates: [   params.lat, params.lng]
      },
      $maxDistance: +params.distance *  1000
    }}
  }
  return query;
}

function drawMarkers(instance){
  let map = GoogleMaps.get('exampleMap');

  let cars = Cars.find(buildQuery(instance.data.search)).fetch();
  if(!cars) cars = [];

  while(instance.data.markersArray.length) { instance.data.markersArray.pop().setMap(null); }
  if(map){
    for(let i = 0; i < cars.length; i++){

      let marker = new google.maps.Marker({
        position: {lat:cars[i].location.coordinates[0], lng: cars[i].location.coordinates[1]},
        map: map.instance
      });

      let infowindow = makeInfoWindow(map, cars[i]);

      instance.data.markersArray.push(marker);
      instance.data.infoWindows.push(infowindow);

      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });

    }
  }   
}