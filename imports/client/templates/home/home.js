import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Cars } from '../../../both/collections/cars.collection';
import * as _ from 'lodash';


import './home.html';

Template.home.onCreated(function carListOnCreated() {

  Meteor.subscribe('myCars');
  Meteor.subscribe('myCarBookings');

});

Template.home.onRendered(function() {

})

Template.home.helpers({

  hasActiveBooking:()=>{
    return Bookings.find({user_id:Meteor.userId(), cancelled_at:{$exists:false}, expires_at:{$gt: + new Date()}}).fetch().length;
  },
});

Template.home.events({
  
});
